﻿// Bare Knuckle Hoof Brawl - Team Dar-Dyl_Lin
// Project 2 for GAME 221
//@ Class ButtonLoadLevel
//@ Author Linda Lane
//@ Date Sept. 9, 2016

// This script is attached to the canvas which has the scene load buttons on it
// The various levels are loaded via the buttons
// The level names are input and attached to each button in the Inspector

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonLoadLevel : MonoBehaviour
{

    public void _LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
        SingletonAudioSource.Instance.PlayEffect(1);           // Add this Audio Source Singleton for the click effect
    }

    public void _LoadSavedGame()
    {
        //SingletonGameController.Instance.Load();
        SingletonAudioSource.Instance.PlayEffect(1);           // Add this Audio Source Singleton for the click effect
    }


    public void _Quit()
    {
        SingletonAudioSource.Instance.PlayEffect(1);           // Add this Audio Source Singleton for the click effect
        Application.Quit();
    }

}
