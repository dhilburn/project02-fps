﻿// Bare Knuckle Hoof Brawl
// LevelManager --  One LevelManager script should be applied to each gameplay scene
// Used to manage information held in each level:  
//      player lives, player health, number of collectibles picked up
// 
// Tags needed: 
// 	1. Bosses to be mind-controlled should be tagged "Boss"
//  2. Player character should be tagged "Player"
//  3. Objects that are placeholders for the respawn points should  be tagged "Respawn"

//@ Author Linda Lane
//@ Class LevelManager
//@ Date 9/11/2015

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class HUDController : MonoBehaviour
{

    [Header("Information from the HUD")]

    // Disable HUD for initial scenes
    [Tooltip("Enter the name of the panel in the Canvas HUD")]
    public Image attacksPanel;

    [Tooltip("Enter the name of the panel in the Canvas HUD")]
    public Image inventoryPanel;

    [Tooltip("Enter the name of the panel in the Canvas HUD")]
    public Image healthTimerPanel;

    public Scrollbar healthBar;
    public Scrollbar hoovesStaminaBar;
    public Scrollbar barbedStaminaBar;
    public Scrollbar bladesStaminaBar;


    public static HUDController Instance;
    //public static HUDController Instance {get; private set;}


    // Use this for initialization
    void Start()
    {
        Instance = this;

        DisableHUD();

        healthBar = healthBar.GetComponent<Scrollbar>();
        hoovesStaminaBar = hoovesStaminaBar.GetComponent<Scrollbar>();
        barbedStaminaBar = barbedStaminaBar.GetComponent<Scrollbar>();
        bladesStaminaBar = bladesStaminaBar.GetComponent<Scrollbar>();

    }


    // Disable HUD for initial scenes

    public void DisableHUD()
    {
        // Get the current scene
        Scene currentScene = SceneManager.GetActiveScene();
        Debug.Log(" Current scene is " + currentScene.name);


        // Disable all HUD images for all scenes that don't use it

        if (currentScene.name == "OO_StartScene" || currentScene.name == "01_Splash_Scene" ||
            currentScene.name == "02_Credits" || currentScene.name == "03_MainMenu" ||
            currentScene.name == "04_Instructions" || currentScene.name == "05_WinScene")
        {
            attacksPanel.enabled = false;
            inventoryPanel.enabled = false;
            healthTimerPanel.enabled = false;
     
        }
         
        if (currentScene.name == "10_Locker_Room_Hub")
        {
            GameObject.Find("TimerText").GetComponent<Text>().enabled = false;
            GameObject.Find("RoundText").GetComponent<Text>().enabled = false;

        }
    }


    void Update()
    {


    }


    // UpdateInventoryBar is called from the InventoryManager script
    public void UpdateInventoryBar(int[] inventoryQuantities, int[] inventoryMaximums)
    {
        string slotTextName;

        for (int i = 0; i < (inventoryQuantities.Length); i++ )
        {
            int adjustCounter;
            adjustCounter = i + 1;
            // set up the names of the slots
            slotTextName = "Slot" + adjustCounter + "Text";

            // check to make sure the values are not greater than the slot maximums in the Inventory Manager

      

            // Input the updated quantities into the slot texts
            GameObject.Find(slotTextName).GetComponent<Text>().text = inventoryQuantities[i].ToString() + "/" + inventoryMaximums[i].ToString();


            //Debug.Log(slotTextName + "equals" + inventoryQuantities[i].ToString());
        }

    }



    public void UpdateHUD(int playerLives, float percentHealth, float percentHoovesStamina, float percentBarbedStamina, float percentBladesStamina, string activeAttack)
    //public void UpdateHUD () 
    {
        GameObject.Find("LivesCount").GetComponent<Text>().text = playerLives.ToString();

        // set up check for one of three options for attacks


        // set up check for playerHealth bar
        healthBar.size = percentHealth;

        hoovesStaminaBar.size = percentHoovesStamina;

        barbedStaminaBar.size = percentBarbedStamina;
    
        bladesStaminaBar.size = percentBladesStamina;
   
    




    }

}
