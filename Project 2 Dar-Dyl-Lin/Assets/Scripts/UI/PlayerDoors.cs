﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerDoors : MonoBehaviour {

  

    // Use this for initialization
    void Start () {
      

    }

    // Update is called once per frame
    void Update () {
	

	}

    void OnTriggerEnter(Collider col)
    {
        
        // Added this collider to negotiate levels 
        if (col.gameObject.CompareTag("Door 0") && SingletonGameController.Instance.bossProgress[0] == false)
            SceneManager.LoadScene("10_Round_1_Scene_Linda");


        // Return to Hub after Boss
        if (col.gameObject.CompareTag("DoorToLockerRoom"))
        {
            if (SingletonGameController.Instance.bossProgress[2])
                SceneManager.LoadScene("05_WinScene");
            else
                SceneManager.LoadScene("10_Locker_Room_Hub");
        }

        // This checks to see if player can advance to next Round before opening the door
        // by making sure the previous world's boss has been defeated


        if (col.gameObject.CompareTag("Door 1") && SingletonGameController.Instance.bossProgress[0] == true)
            SceneManager.LoadScene("10_Round_2_Scene_Dylan");

        if (col.gameObject.CompareTag("Door 2") && SingletonGameController.Instance.bossProgress[1] == true)
            SceneManager.LoadScene("10_Round_3_Scene_Darrick");
      

    }
}
