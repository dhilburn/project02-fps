﻿using UnityEngine;
using System.Collections;

public class PickupsController : MonoBehaviour {

    [Tooltip("Usual number is between 1000-3000.")]
    public int pickupSpawnRate;
    private int randomNumber;
    public GameObject randomPickup;

    // Use this for initialization
    void Start () {

        // all pickups should have disabled renderers
        GameObject.Find("Ear").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("Grass Chaw").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("Goat Milk").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("Goat Oil").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("GoatOyster").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("WhiteTowel").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("Banjo Music").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("Brick").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("GoatSkin").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("EmptyBeerCan").GetComponent<MeshRenderer>().enabled = false;

    }

    // Update is called once per frame
    void Update ()
    {

        // Make pickup items spawn at random times
        randomNumber = (int)(Random.value * pickupSpawnRate);

        //Debug.Log("Random Int is" + randomNumber);

        switch (randomNumber)
        {
            case 5:

                randomPickup = GameObject.Find("Grass Chaw");
                randomPickup.GetComponent<MeshRenderer>().enabled = true;
               // Debug.Log("Spawned a" + randomPickup);
                break;

            case 10:
            case 11:
            case 12:
                randomPickup = GameObject.Find("Goat Milk");
                randomPickup.GetComponent<MeshRenderer>().enabled = true;
               // Debug.Log("Spawned a" + randomPickup);

                break;

            case 20:
            case 21:
            case 22:
                randomPickup = GameObject.Find("Goat Oil");
                randomPickup.GetComponent<MeshRenderer>().enabled = true;
               // Debug.Log("Spawned a" + randomPickup);

                break;

            case 30:
                randomPickup = GameObject.Find("GoatOyster");
                randomPickup.GetComponent<MeshRenderer>().enabled = true;
                //Debug.Log("Spawned a" + randomPickup);

                break;

            case 40:
            case 41:
                randomPickup = GameObject.Find("WhiteTowel");
                randomPickup.GetComponent<MeshRenderer>().enabled = true;
               // Debug.Log("Spawned a" + randomPickup);

                break;

            case 50:
            case 51:
                randomPickup = GameObject.Find("Banjo Music");
                randomPickup.GetComponent<MeshRenderer>().enabled = true;
                //Debug.Log("Spawned a" + randomPickup);

                break;
            case 60:
                randomPickup = GameObject.Find("Brick");
                randomPickup.GetComponent<MeshRenderer>().enabled = true;
                //Debug.Log("Spawned a" + randomPickup);

                break;
            case 70:
            case 71:
            case 72:
                randomPickup = GameObject.Find("GoatSkin");
                randomPickup.GetComponent<MeshRenderer>().enabled = true;
                //Debug.Log("Spawned a" + randomPickup);

                break;
            case 80:
            case 81:
            case 82:
                randomPickup = GameObject.Find("EmptyBeerCan");
                randomPickup.GetComponent<MeshRenderer>().enabled = true;
                //Debug.Log("Spawned a" + randomPickup);

                break;
        }
    }

   
}
