﻿// Bare Knuckle Hoof Brawl - Team Dar-Dyl_Lin
// Project 2 for GAME 221
//@ Class CheatManager
//@ Author Linda Lane
//@ Date Sept. 9, 2016

// CheatManager is put on an Empty Game Object in each scene
// Public variable "nextScene" requires entering the name of the scnen after this you wish to go to

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CheatManager : MonoBehaviour {

    LevelManager cheatLevelManager;

    public string nextScene;

	// Use this for initialization
	void Start () {

        // Set up easy access to the Level Manager script
        cheatLevelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        // Keep track of the current scene
        Scene currentScene = SceneManager.GetActiveScene();
    }
	
	// Update is called once per frame
	void Update ()
    {
        // Advance scenes with N key
        if (Input.GetKeyDown(KeyCode.N))
        {
            Debug.Log("Advancing to next screen.");
            SceneManager.LoadScene(nextScene);
        }

       
        // Kill the boss with B key
        if (Input.GetKeyDown(KeyCode.B))
        {
            Object bossToKill = GameObject.FindWithTag("Boss");

            if (bossToKill != null && bossToKill.name == "Round1Boss")
            {
                Destroy(bossToKill);
                SingletonGameController.Instance.bossProgress[0] = true;
                Debug.Log("You won Round 1.");

            }

            if (bossToKill != null && bossToKill.name == "Round2Boss")
            {
                Destroy(bossToKill);
                SingletonGameController.Instance.bossProgress[1] = true;
                Debug.Log("You won Round 2.");

            }

            if (bossToKill != null && bossToKill.name == "Round3Boss")
            {
                Destroy(bossToKill);
                SingletonGameController.Instance.bossProgress[2] = true;
                Debug.Log("You won Round 3.");

            }

            cheatLevelManager.UpdateBoss();

        }

        // Increase or decrease player health with + or - keys
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            cheatLevelManager.LoseHealth(10);
            Debug.Log("Player lost 10 hp");
        }

        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            cheatLevelManager.AddHealth(10);
            Debug.Log("Player gained 10 hp");
        }


        // Increase or decrease different player staminas with keys
        if (Input.GetKeyDown(KeyCode.I))
        {
            if(cheatLevelManager.activeAttack == "Hooves")
            {
                cheatLevelManager.LoseHoovesStamina(10);
            }
            if (cheatLevelManager.activeAttack == "Barbed")
            {
                cheatLevelManager.LoseBarbedStamina(10);
            }
            if (cheatLevelManager.activeAttack == "Blades")
            {
                cheatLevelManager.LoseBladesStamina(10);
            }
            Debug.Log("Player lost " + cheatLevelManager.activeAttack + " 10 stamina");
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            if (cheatLevelManager.activeAttack == "Hooves")
            {
                cheatLevelManager.AddHoovesStamina(10);
            }
            if (cheatLevelManager.activeAttack == "Barbed")
            {
                cheatLevelManager.AddBarbedStamina(10);
            }
            if (cheatLevelManager.activeAttack == "Blades")
            {
                cheatLevelManager.AddBladesStamina(10);
            }
            Debug.Log("Player gained 10 stamina");
        }

    }
}
