﻿// Bare Knuckle Hoof Brawl - Team Dar-Dyl_Lin
// Project 2 for GAME 221
//@ Class SingletonGameController
//@ Author Linda Lane
//@ Date Sept. 9, 2016

// This script saves the game info and loads the previous save info

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SingletonGameController : MonoBehaviour
{

    // Set up singleton variable
    public static SingletonGameController Instance { get; private set; }

    // Set up win/loss flag for each boss
    // First Boss is bossProgress[0], Second Boss is bossProgress[1], 3rd Boss is bossProgress[2]
    public bool[] bossProgress = new bool[3];


    public bool CLEAR_ALL_SAVES = false;


    // Use this for initialization
    void Start()
    {

        // Initialize all values
        for (int i = 0; i < bossProgress.Length; i++)
        {

            bossProgress[i] = false;
        }

        if (CLEAR_ALL_SAVES)
        {
            // Set the boss progress in playerprefs
            for (int i = 0; i < bossProgress.Length; i++)
            {
                PlayerPrefs.DeleteKey(("Boss " + i));
            }


            CLEAR_ALL_SAVES = false;
        }

        // Load saved game if there is one
        Load();


    }



    void Awake()
    {
        // Check for other instances of me and if they exist, destroy me
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        // If no other, make me the instance and don't destroy me between scenes
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void Load()
    {
        Debug.Log("Loading previous game if it exists");

        // Does playerprefs have any boss outcomes stored? 
        // If so, use them
           // Load the boss progress
            for (int i = 0; i < bossProgress.Length; i++)
            {

                bossProgress[i] = PlayerPrefsIntToBool(PlayerPrefs.GetInt(("Boss " + i)));
            }


            int lastDefeatedBoss = -1;
           
        //Destroy the door for the next level the player has to complete
            for (int i = 0; i < bossProgress.Length; i++)
            {
                if (bossProgress[i] == true)
                    lastDefeatedBoss = i;
            }

            //Check if we have defeated the first boss
            if (lastDefeatedBoss != -1)
            {
                // make sure we have not "won" the game by defeating the final boss
                if (lastDefeatedBoss != bossProgress.Length - 1)
                {
                    Destroy(GameObject.Find(("Door " + (lastDefeatedBoss + 1))));
                }
            }
    }

    public void Save()
    {

        // Set the boss progress in playerprefs
        for (int i = 0; i < bossProgress.Length; i++)
        {
            PlayerPrefs.SetInt(("Boss " + i), PlayerPrefsBoolToInt(bossProgress[i]));
        }

    }

    int PlayerPrefsBoolToInt(bool boolToConvert)
    {
        if (boolToConvert)
            return 1;
        else
            return 0;
    }

    bool PlayerPrefsIntToBool(int intToConvert)
    {
        if (intToConvert == 1)
            return true;
        else
            return false;
    }



    public void CompleteBoss(int bossNum)
    {
        bossProgress[bossNum] = true;
        Save();
    }
}
