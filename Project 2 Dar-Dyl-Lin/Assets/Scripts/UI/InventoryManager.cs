﻿using UnityEngine;
using System.Collections;

public class InventoryManager : MonoBehaviour {

   private enum InventoryItems { EARS, GRASSCHAW, MILK, OIL, OYSTERS, TOWEL, BANJO, BRICK, GOAT_SKIN, BEER_CAN};

    public int numberInventorySlots = 10;
    public int maxInStack;
    public int numberInStack;

    LevelManager levelManager;

    // Create the array for handling the inventory
    // Inventory has 10 slots, each slot has a stack amount [0] and a stack limit [1] assigned to it
    // Each slot has been  assigned to a particular inventory item (to make this first implementation easier)
    // The enum declared has the slot assignments and order in inventory of the items

    [Header ("Inventory Arrays")]
    [Tooltip("These maximums are for the size of the inventory.  For this game they should be 10.")]
    public int[] inventoryAmounts = new int[10];
    public int[] inventoryStackMax = new int[10];

	// Use this for initialization
	void Start ()
    {

        // Set up easy access to the Level Manager script
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();

        // Initialize the inventory

        InitializeInventory();
        InitializeInventoryMaximums();

        // Update the HUD inventory bar

        UpdateHUDInventory();


	}
	
	// Update is called once per frame
	void Update ()
    {

       
    }



    public void UpdateHUDInventory()
    {
        for (int i = 0; i < (inventoryAmounts.Length); i++)

        {
            if (inventoryAmounts[i] > inventoryStackMax[i])
            {
                inventoryAmounts[i] = inventoryStackMax[i];
            }
            if (inventoryAmounts[i] <= 0)
            {
                inventoryAmounts[i] = 0;
            }


        }

        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateInventoryBar(inventoryAmounts, inventoryStackMax);
    }
    public void InitializeInventory()
    {
        // Initialize all inventory values:  [starting amount in stack]
        inventoryAmounts[(int)InventoryItems.EARS] = 0;
        inventoryAmounts[(int)InventoryItems.GRASSCHAW] = 1;
        inventoryAmounts[(int)InventoryItems.MILK] = 1;
        inventoryAmounts[(int)InventoryItems.OIL] = 1;
        inventoryAmounts[(int)InventoryItems.OYSTERS] = 0;
        inventoryAmounts[(int)InventoryItems.TOWEL] = 1;
        inventoryAmounts[(int)InventoryItems.BANJO] = 0;
        inventoryAmounts[(int)InventoryItems.BRICK] = 0;
        inventoryAmounts[(int)InventoryItems.GOAT_SKIN] = 0;
        inventoryAmounts[(int)InventoryItems.BEER_CAN] = 0;

    }

    public void InitializeInventoryMaximums()
    {
        // Initialize all inventory values:  [stack limit]
        inventoryStackMax[(int)InventoryItems.EARS] = 2;
        inventoryStackMax[(int)InventoryItems.GRASSCHAW] = 10;
        inventoryStackMax[(int)InventoryItems.MILK] = 10;
        inventoryStackMax[(int)InventoryItems.OIL] = 10;
        inventoryStackMax[(int)InventoryItems.OYSTERS] = 2;
        inventoryStackMax[(int)InventoryItems.TOWEL] = 99;
        inventoryStackMax[(int)InventoryItems.BANJO] = 1;
        inventoryStackMax[(int)InventoryItems.BRICK] = 99;
        inventoryStackMax[(int)InventoryItems.GOAT_SKIN] = 5;
        inventoryStackMax[(int)InventoryItems.BEER_CAN] = 10;
    }

    public void _EmptyBeerCan()
    { 
        if(levelManager.levelNumber == 1)
        levelManager.AddHoovesStamina(3);
        else if (levelManager.levelNumber == 2)
        levelManager.AddBarbedStamina(3);
        else if (levelManager.levelNumber == 3)
        levelManager.AddBladesStamina(3);
    }

    public void _Milk()
    {
        levelManager.AddHealth(5);
    }

    public void _GoatOil()
    {
        if (levelManager.levelNumber == 2 || levelManager.levelNumber == 3)
            levelManager.AddBarbedStamina(5);
    }

    public void _GoatOysters()
    {
        if (levelManager.levelNumber == 3)
            levelManager.AddBladesStamina(20);
    }

    public void _Towel()
    {
        SingletonAudioSource.Instance.PlayEffect(8);
    }
    public void _Banjo()
    {
        SingletonAudioSource.Instance.PlayEffect(8);
    }
    public void _Brick()
    {
        SingletonAudioSource.Instance.PlayEffect(8);

    }
    public void _GoatSkin()
    {
        levelManager.AddHealth(10);
    }

}
