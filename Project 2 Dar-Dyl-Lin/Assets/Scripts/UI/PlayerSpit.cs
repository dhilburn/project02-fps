﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerSpit : MonoBehaviour {

    private enum InventoryItems { EARS, GRASSCHAW, MILK, OIL, OYSTERS, TOWEL, BANJO, BRICK, GOAT_SKIN, BEER_CAN };


    InventoryManager invManager;


    [Tooltip("Manually assign these from objects named the same in the hierarchy")]
    public Rigidbody EarProjectile;
    public Rigidbody GrassChawProjectile;



    [Tooltip("Starting value is around 5")]
    public int projectileForce;         //set the speed

	// Use this for initialization
	void Start () {

        EarProjectile = EarProjectile.GetComponent<Rigidbody>();
        GrassChawProjectile = GrassChawProjectile.GetComponent<Rigidbody>();

        // Set up easy access to the InventoryManager script
        invManager = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();

    }
	

	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (invManager.inventoryAmounts[(int)InventoryItems.EARS] > 0)
            {
                _SpitEars();
                --invManager.inventoryAmounts[(int)InventoryItems.EARS];
                invManager.UpdateHUDInventory();
            }
          }

        

        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            if (invManager.inventoryAmounts[(int)InventoryItems.GRASSCHAW] > 0)
            {
                _SpitChaw();
                --invManager.inventoryAmounts[(int)InventoryItems.GRASSCHAW];
                invManager.UpdateHUDInventory();
            }
        }
    }

    public void _SpitEars()
    {
       
        // Play the spitting sound effect
        //SingletonAudioSource.Instance.PlayEffect(11);

        Rigidbody earProjectileInstance = Instantiate(EarProjectile, transform.position, transform.rotation) as Rigidbody;
        earProjectileInstance.velocity = transform.TransformDirection(Vector3.forward * projectileForce);

    }

    public void _SpitChaw()
    {
        // Play the spitting sound effect
        //SingletonAudioSource.Instance.PlayEffect(11);

        Rigidbody projectileInstance = Instantiate(GrassChawProjectile, transform.position, transform.rotation) as Rigidbody;
        projectileInstance.velocity = transform.TransformDirection(Vector3.forward * projectileForce);
    }
}
