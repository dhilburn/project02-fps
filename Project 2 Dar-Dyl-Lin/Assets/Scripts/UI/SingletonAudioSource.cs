﻿// Bare Knuckle Hoof Brawl - Team Dar-Dyl_Lin
// Project 2 for GAME 221
//@ Class SingletonAudioSource
//@ Author Linda Lane
//@ Date Sept. 9, 2016

// This script is used to control the background music and sound effects through
// all levels in the game.
// Can be used to call the PlayAudio() function in other scripts
//  with the command SingletonAudioSource.Instance.PlayAudio(SingletonAudioSource.Instance.soundClip);

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SingletonAudioSource : MonoBehaviour {

    //  Buttons
    public AudioClip buttonSelect;
    public AudioSource buttonSource;

    // Background Music
    public AudioSource backgroundSource;

    // Sounds used every level
    // We need to add sounds as we decide on them
    public AudioSource bossNoiseSource;
    public AudioSource playerNoiseSource;
    public AudioSource pickupSomething;
    public AudioSource useSomething;

    // Static singleton variable
    public static SingletonAudioSource Instance { get; private set; }


    // Singleton Housekeeping
    // We want this to happen as soon as the game first takes breath!
	void Awake ()
    {
	
        // Check if there are any other instances conflicting with this script's instance
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }

        // Save a reference to the SingletonAudioSource component as the singleton instance
        Instance = this;

        // Make sure this object is not destroyed between scenes
        DontDestroyOnLoad(gameObject);
	}
	
	//Instance method: PlayBackground ()
    // Plays background music in each scene
    // This method can be accessed through the singleton Instance
    public void PlayBackground(AudioClip music)
    {
        backgroundSource.clip = music;
        backgroundSource.Play();
        backgroundSource.loop = true;
    } // end PlayBackground

    // Instance method: StopBackground()
    // Stops background music
    public void StopBackground()
    {
        backgroundSource.Stop ();
    }

    public void PlayEffect(int effect)
    {
        // Keep track of current scene
        Scene currentScene = SceneManager.GetActiveScene();

        switch (effect)
        {
            case 1:         // button clicky noise
                buttonSource.clip = buttonSelect;
                buttonSource.Play();
                buttonSource.loop = false;
                break;

            case 2:         //  boss attack noises 
                if (SingletonAudioSource.Instance != null)
                {

                    if (currentScene.name == "10_Round_1_Scene")
                        bossNoiseSource.clip = Resources.Load<AudioClip>("Audio/Boss1_Attack");
                    else if (currentScene.name == "10_Round_2_Scene")
                        bossNoiseSource.clip = Resources.Load<AudioClip>("Audio/Boss2_Attack");
                    else if (currentScene.name == "10_Round_3_Scene")
                        bossNoiseSource.clip = Resources.Load<AudioClip>("Audio/Boss3_Attack");

                    bossNoiseSource.Play();
                    bossNoiseSource.loop = false;
                }
                break;

            case 3:         //  player attack noises 
                if (SingletonAudioSource.Instance != null)
                {
                    if (currentScene.name == "10_Round_1_Scene")
                        playerNoiseSource.clip = Resources.Load<AudioClip>("Audio/Player_Attack1");
                    else if (currentScene.name == "10_Round_2_Scene")
                        playerNoiseSource.clip = Resources.Load<AudioClip>("Audio/Player_Attack2");
                    else if (currentScene.name == "10_Round_3_Scene")
                        playerNoiseSource.clip = Resources.Load<AudioClip>("Audio/Player_Attack3");

                    playerNoiseSource.Play();
                    playerNoiseSource.loop = false;
                }
                break;

            case 4:         //  Boss death noises
                if (SingletonAudioSource.Instance != null)
                {

                    if (currentScene.name == "10_Round_1_Scene")
                        bossNoiseSource.clip = Resources.Load<AudioClip>("Audio/Boss1_Death");
                    else if (currentScene.name == "10_Round_2_Scene")
                        bossNoiseSource.clip = Resources.Load<AudioClip>("Audio/Boss2_Death");
                    else if (currentScene.name == "10_Round_3_Scene")
                        bossNoiseSource.clip = Resources.Load<AudioClip>("Audio/Boss3_Death");

                    bossNoiseSource.Play();
                    bossNoiseSource.loop = false;
                }
                break;

            case 5:         // Item Pickup noise
                playerNoiseSource.clip = Resources.Load<AudioClip>("Audio/Item_Pickup");
                playerNoiseSource.Play();
                playerNoiseSource.loop = false;
                break;

            case 6:         // Item use noise
                playerNoiseSource.clip = Resources.Load<AudioClip>("Audio/Item_Use");
                playerNoiseSource.Play();
                playerNoiseSource.loop = false;
                break;

            case 7:         // the Bell for the round begin and end
                playerNoiseSource.clip = Resources.Load<AudioClip>("Audio/TripleBell");
                playerNoiseSource.Play();
                playerNoiseSource.loop = false;
                break;

            case 8:         // the player death noise 
                playerNoiseSource.clip = Resources.Load<AudioClip>("Audio/Player_Death");
                playerNoiseSource.Play();
                playerNoiseSource.loop = false;
                break;

            case 9:         // the player block noise 
                playerNoiseSource.clip = Resources.Load<AudioClip>("Audio/Player_Block");
                playerNoiseSource.Play();
                playerNoiseSource.loop = false;
                break;

            case 10:         // the player something else noise 
                playerNoiseSource.clip = Resources.Load<AudioClip>("Audio/Player_Hurt");
                playerNoiseSource.Play();
                playerNoiseSource.loop = false;
                break;

            case 11:         // the player Spit Ears
                playerNoiseSource.clip = Resources.Load<AudioClip>("Audio/Spitting");
                playerNoiseSource.Play();
                playerNoiseSource.loop = false;
                break;

            

            default:
                break;
        }
    } // end PlayEffect()
}
