﻿using UnityEngine;
using System.Collections;

public class PickupsBehavior : MonoBehaviour {

    private enum InventoryItems { EARS, GRASSCHAW, MILK, OIL, OYSTERS, TOWEL, BANJO, BRICK, GOAT_SKIN, BEER_CAN };


    InventoryManager invManager;

	// Use this for initialization
	void Start () {

        // Set up easy access to the InventoryManager script
        invManager = GameObject.Find("InventoryManager").GetComponent<InventoryManager>();

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {

        if (col.gameObject.CompareTag("Player"))
            GetComponent<MeshRenderer>().enabled = false;

        if(name == "Ear")
        {
            ++invManager.inventoryAmounts[(int)InventoryItems.EARS];
            invManager.UpdateHUDInventory();
        }
        if (name == "Grass Chaw")
        {
            ++invManager.inventoryAmounts[(int)InventoryItems.GRASSCHAW];
            invManager.UpdateHUDInventory();
        }
        if (name == "Goat Milk")
        {
            ++invManager.inventoryAmounts[(int)InventoryItems.MILK];
            invManager.UpdateHUDInventory();
        }
        if (name == "Goat Oil")
        {
            ++invManager.inventoryAmounts[(int)InventoryItems.OIL];
            invManager.UpdateHUDInventory();
        }
        if (name == "GoatOyster")
        {
            ++invManager.inventoryAmounts[(int)InventoryItems.OYSTERS];
            invManager.UpdateHUDInventory();
        }
        if (name == "WhiteTowel")
        {
            ++invManager.inventoryAmounts[(int)InventoryItems.TOWEL];
            invManager.UpdateHUDInventory();
        }
        if (name == "Banjo Music")
        {
            ++invManager.inventoryAmounts[(int)InventoryItems.BANJO];
            invManager.UpdateHUDInventory();
        }
        if (name == "Brick")
        {
            ++invManager.inventoryAmounts[(int)InventoryItems.BRICK];
            invManager.UpdateHUDInventory();
        }
        if (name == "GoatSkin")
        {
            ++invManager.inventoryAmounts[(int)InventoryItems.GOAT_SKIN];
            invManager.UpdateHUDInventory();
        }
        if (name == "EmptyBeerCan")
        {
            ++invManager.inventoryAmounts[(int)InventoryItems.BEER_CAN];
            invManager.UpdateHUDInventory();
        }
    }
}
