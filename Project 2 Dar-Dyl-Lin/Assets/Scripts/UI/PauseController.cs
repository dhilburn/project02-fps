﻿// BareKnuckleHoof Brawl
// 
// PauseController script is a singleton placed on the Canvas of the scene that first uses the Pause, the 10_HubWorld
// This script is used to Pause and un-Pause game play, and gives options to return to other worlds

//@ Author Linda Lane
//@ Class PauseController
//@ Date 9/11/2015

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseController : MonoBehaviour
{

    [Header("Paused Screen Information")]
    public bool gameIsPaused;
    public Text pauseText;

    private Button playButton;
    private Button quitButton;
    private Button mainMenuButton;
    private Button goToHubButton;
    private GUIElement leftButtonPanel;
    private GUIElement rightButtonPanel;

    //public static PauseController Instance;
    // Set up singleton variable
    public static PauseController Instance { get; private set; }


    // Use this for initialization
    void Start()
    {
        Instance = this;

        // Set up text and 4 buttons on the Pause screen to pause the game
        pauseText = GameObject.Find("Pause Text").GetComponent<Text>();
        playButton = GameObject.Find("Play_Button").GetComponentInChildren<Button>();
        mainMenuButton = GameObject.Find("Main_Menu_Button").GetComponentInChildren<Button>();
        goToHubButton = GameObject.Find("Return_Hub_Button").GetComponentInChildren<Button>();
        quitButton = GameObject.Find("Quit_Game_Button").GetComponentInChildren<Button>();

        DisablePauseScreen();

    }


    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!gameIsPaused)
            {
                Time.timeScale = 0;
                gameIsPaused = true;
                Debug.Log("Game is paused");
                EnablePauseScreen();

            }
            else
            {
                Time.timeScale = 1;
                gameIsPaused = false;
                Debug.Log("Game is playing");
                DisablePauseScreen();

            }
        }

        // Make it so if they click on the MainMenu screen, the Pause screen is disabled


    } // end Update

    public void EnablePauseScreen()
    {
        pauseText.enabled = true;
        // make all pause menu buttons visible and interactable
        playButton.interactable = true;
        playButton.GetComponentInChildren<Text>().text = "Resume Game";

        quitButton.interactable = true;
        quitButton.GetComponentInChildren<Text>().text = "Quit Game";

        mainMenuButton.interactable = true;
        mainMenuButton.GetComponentInChildren<Text>().text = "Main Menu";

        goToHubButton.interactable = true;
        goToHubButton.GetComponentInChildren<Text>().text = "Locker Room";
    }

    public void DisablePauseScreen()
    {
        pauseText.enabled = false;
        // make all the pause menu buttons invisible and non-interactable

        playButton.interactable = false;
        playButton.GetComponentInChildren<Text>().text = "";

        quitButton.interactable = false;
        quitButton.GetComponentInChildren<Text>().text = "";

        mainMenuButton.interactable = false;
        mainMenuButton.GetComponentInChildren<Text>().text = "";

        goToHubButton.interactable = false;
        goToHubButton.GetComponentInChildren<Text>().text = "";
    }

    public void _GoToMainMenu()
    {
        DisablePauseScreen();
        gameIsPaused = false;
        SceneManager.LoadScene("03_MainMenu");
        SingletonAudioSource.Instance.PlayEffect(1);           // Add this Audio Source Singleton for the click effect


    }

    public void _ReturnToHub()
    {
        DisablePauseScreen();
        gameIsPaused = false;
        SceneManager.LoadScene("10_Locker_Room_Hub");
        SingletonAudioSource.Instance.PlayEffect(1);           // Add this Audio Source Singleton for the click effect

    }

    public void _ResumeGame()
    {
        Time.timeScale = 1;
        gameIsPaused = false;
        Debug.Log("Game is playing");
        SingletonAudioSource.Instance.PlayEffect(1);           // Add this Audio Source Singleton for the click effect
        DisablePauseScreen();
    }

    public void _Quit()
    {
        SingletonAudioSource.Instance.PlayEffect(1);           // Add this Audio Source Singleton for the click effect
        Application.Quit();
    }

}
