﻿// Bare Knuckle Hoof Brawl
// LevelManager --  One LevelManager script should be applied to each gameplay scene
// Used to manage information held in each level:  
//      player lives, player health, number of collectibles picked up
// 
// Tags needed: 
// 	1. Bosses to be mind-controlled should be tagged "Boss"
//  2. Player character should be tagged "Player"
//  3. Objects that are placeholders for the respawn points should  be tagged "Respawn"
// 

//@ Author Linda Lane
//@ Class LevelManager
//@ Date 9/10/2015

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{

    // HUD variables
    private int playerLives;            // 3 lives (red heart)  //hardcoded value due to design specification
    private float playerHealth;           // Health amount
    private float playerHoovesStamina;    // Hooves Stamina variable amount
    private float playerBarbedStamina;    // Barbed Stamina variable amount
    private float playerBladesStamina;    // Blades Stamina variable amount

    private float percentHealth;            // Health percent for bar
    private float percentHoovesStamina;    // Hooves Stamina percent for bar
    private float percentBarbedStamina;    // Barbed Stamina percent for bar
    private float percentBladesStamina;    // Blades Stamina percent for bar


    [Tooltip("Maximum player health. Suggested starting value is 100.")]
    public float maxPlayerHealth;       // Designer input variable
    [Tooltip("Maximum player stamina. Suggested starting value is 100.")]
    public float maxPlayerStamina;      // Designer input variable

    [HideInInspector]
    public string activeAttack;        // The active attack upon entering the level
    
    // Inspector variables
    [Tooltip("Time Limit for Round. Number must be in seconds.")]
    public float roundTime;     // Timer in seconds for the round - Designer variable
    private float timeLeft;
    public Text timerText;
    public Text roundText;

    [Tooltip("Level in game. Used for debug purposes.")]
    public int levelNumber;             // identify which level by number IN THE INSPECTOR

    [Tooltip("Boss flag. Used for debug purposes.")]
    public bool bossWin;
  

    // Player and level variables
    private Vector3 respawnPoint;       // Location to respawn in level
    private GameObject player;          // identify player
    

    public AudioClip backgroundMusic;   // Set up background music

    GameObject hoovesAttack;
    GameObject barbedAttack;
    GameObject bladesAttack;

    // Use this for initialization
    void Start()
    {

        // Setup defaults
        playerLives = 3;
        bossWin = false;

        InitializeRound();


        // Keep track of the current scene
        Scene currentScene = SceneManager.GetActiveScene();

        //Debug.Log("This scene is" + currentScene.name);

       
       GameObject.Find("RoundText").GetComponent<Text>().text = "Round: " + levelNumber;

       GameObject.Find("DoorToLockerRoom").GetComponent<MeshRenderer>().enabled = false;



        // Play appropriate background music for each level
        if (SingletonAudioSource.Instance != null)
        {
            if (currentScene.name == "00_StartScene" || currentScene.name == "01_Splash_Scene" || currentScene.name == "02_Credits" ||
                currentScene.name == "03_MainMenu" || currentScene.name == "04_Instructions" || currentScene.name == "04_WinScene" ||
                currentScene.name == "10_Locker_Room_Hub")
            {
                backgroundMusic = Resources.Load<AudioClip>("Audio/Synth Bass 16 bars");
            }
            else if (currentScene.name == "10_Round_1_Scene_Linda")
            {
                backgroundMusic = Resources.Load<AudioClip>("Audio/345 Street Justice");
            }
            else if (currentScene.name == "10_Round_2_Scene_Dylan")
            {
                backgroundMusic = Resources.Load<AudioClip>("Audio/Retinal Racers");
            }
            else if (currentScene.name == "10_Round_3_Scene_Darrick")
            {
                backgroundMusic = Resources.Load<AudioClip>("Audio/taiko-drums");
            }

            SingletonAudioSource.Instance.PlayBackground(backgroundMusic);
        }


        // Set the health and stamina bar calculation up

        percentHealth = playerHealth / maxPlayerHealth;
        //Debug.Log("Percent Health is " + percentHealth);

        percentHoovesStamina = playerHoovesStamina / maxPlayerStamina;
       // Debug.Log("Percent Hooves Stamina is " + percentHoovesStamina);

        percentBarbedStamina = playerBarbedStamina / maxPlayerStamina;
       // Debug.Log("Percent Barbed Stamina is " + percentBarbedStamina);

        percentBladesStamina = playerBladesStamina / maxPlayerStamina;
       // Debug.Log("Percent Blades Stamina is " + percentBladesStamina);

        // Update the HUD 
        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

        // Find the  Attack Sprites to change their color when one is the activeAttack

        hoovesAttack = GameObject.Find("HoovesAttackSprite");
        barbedAttack = GameObject.Find("BarbedAttackSprite");
        bladesAttack = GameObject.Find("BladesAttackSprite");


        // Set up spawn point
        respawnPoint = GameObject.FindWithTag("Respawn").transform.position;

        // identify player
        player = GameObject.FindWithTag("Player");
   }

    void InitializeRound()
    {

        playerHealth = maxPlayerHealth;
        playerHoovesStamina = maxPlayerStamina;

        // Check to make sure player has unlocked advanced attacks by being in round 2 or 3

        if (levelNumber == 2 || levelNumber == 3)
        {
            playerBarbedStamina = maxPlayerStamina;
        }
        else
        {
            playerBarbedStamina = 0.0f;
        }

        if (levelNumber == 3)
        {
            playerBladesStamina = maxPlayerStamina;
        }
        else
        {
            playerBladesStamina = 0.0f;
        }

        // Set up the active attack and the round timer
        activeAttack = "Hooves";
        timeLeft = roundTime;
       

        // Set up timer
        InvokeRepeating("CountdownTimer", 2.0f, 0.1f);

        // Update the HUD
        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

    }
    void Update ()
    {
        if (Input.GetKeyDown (KeyCode.Alpha1))
        {
            activeAttack = "Hooves";
            //Debug.Log("Hooves Attack is active.");
           
                GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);
             
                
            if (playerHoovesStamina > 0)
            {
                hoovesAttack.GetComponent<Image>().color = new Color(255, 0, 0);
            }
            else
            {
                hoovesAttack.GetComponent<Image>().color = new Color(113, 113, 46);
            }
            if (playerBarbedStamina > 0)
            {
                barbedAttack.GetComponent<Image>().color = new Color(255, 255, 255);
            }
            else
            {
                barbedAttack.GetComponent<Image>().color = new Color(113, 113, 46);
            }
            if (playerHoovesStamina > 0)
            {
                bladesAttack.GetComponent<Image>().color = new Color(255, 255, 255);
            }
            else
            {
                bladesAttack.GetComponent<Image>().color = new Color(113, 113, 46);
            }

        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && (levelNumber != 1))
        {
            activeAttack = "Barbed";
           // Debug.Log("Barbed Attack is active.");
            GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);


            if (playerHoovesStamina > 0)
            {
                hoovesAttack.GetComponent<Image>().color = new Color(255, 255, 55);
            }
            else
            {
                hoovesAttack.GetComponent<Image>().color = new Color(113, 113, 46);
            }
            if (playerBarbedStamina > 0)
            {
                barbedAttack.GetComponent<Image>().color = new Color(255, 0, 0);
            }
            else
            {
                barbedAttack.GetComponent<Image>().color = new Color(113, 113, 46);
            }
            if (playerHoovesStamina > 0)
            {
                bladesAttack.GetComponent<Image>().color = new Color(255, 255, 255);
            }
            else
            {
                bladesAttack.GetComponent<Image>().color = new Color(113, 113, 46);
            }


        }
        if (Input.GetKeyDown(KeyCode.Alpha3) && (levelNumber == 3))
        {
            activeAttack = "Blades";
            //Debug.Log("Blades Attack is active.");
            GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

            if (playerHoovesStamina > 0)
            {
                hoovesAttack.GetComponent<Image>().color = new Color(255, 255, 255);
            }
            else
            {
                hoovesAttack.GetComponent<Image>().color = new Color(113, 113, 46);
            }
            if (playerBarbedStamina > 0)
            {
                barbedAttack.GetComponent<Image>().color = new Color(255, 255, 255);
            }
            else
            {
                barbedAttack.GetComponent<Image>().color = new Color(113, 113, 46);
            }
            if (playerHoovesStamina > 0)
            {
                bladesAttack.GetComponent<Image>().color = new Color(255, 0, 0);
            }
            else
            {
                bladesAttack.GetComponent<Image>().color = new Color(113, 113, 46);
            }


        }

    

    }


    public void LoseHealth(int amount)
    {
        // Decrease health by amount
        playerHealth = playerHealth - amount;

        // Calculate the percentage health for the healthbar
        percentHealth = playerHealth / maxPlayerHealth;
        //Debug.Log("Percent Health is " + percentHealth);

        // Update the HUD image
        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

        if (playerHealth <= 0)
        {
            // Play the death sound
           SingletonAudioSource.Instance.PlayEffect(8);
            // Wait a second to let player watch their health be zero and let it sink in that the player has died
            Invoke("Die", 0.5f);
          
        }
    }

    public void LoseHoovesStamina(int amount)
    {
        // Decrease stamina by amount
        playerHoovesStamina = playerHoovesStamina - amount;

        // Calculate the percentage Hooves stamina for the stamina bar
        percentHoovesStamina = playerHoovesStamina / maxPlayerStamina;

        // Update the HUD image
        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

        if (playerHoovesStamina <= 0)
        {
            playerHoovesStamina = 0;
            
        }
    }

    public void LoseBarbedStamina(int amount)
    {
        // Decrease stamina by amount
        playerBarbedStamina = playerBarbedStamina - amount;

        // Calculate the percentage Barbed stamina for the stamina bar
        percentBarbedStamina = playerBarbedStamina / maxPlayerStamina;

        // Update the HUD image
        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

        if (playerBarbedStamina <= 0)
        {
            playerBarbedStamina = 0;

        }
    }

    public void LoseBladesStamina(int amount)
    {
        // Decrease stamina by amount
        playerBladesStamina = playerBladesStamina - amount;

        // Calculate the percentage Bladess stamina for the stamina bar
        percentBladesStamina = playerBladesStamina / maxPlayerStamina;

        // Update the HUD image
        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

        if (playerBladesStamina <= 0)
        {
            playerBladesStamina = 0;

        }
    }

    public void AddHealth(int amount)
    {
        // Check to see if player health is low
        if (playerHealth < maxPlayerHealth)
        {
            // If player Health is down, add hp
            playerHealth = playerHealth + amount;

            if (playerHealth > maxPlayerHealth)
            {
                playerHealth = maxPlayerHealth;
             //   Debug.Log("Player health is maxed");
            }

            // Calculate the percentage health for the healthbar
            percentHealth = playerHealth / maxPlayerHealth;
            //Debug.Log("Percent Health is " + percentHealth);

            // Update the HUD image
            GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

        }
    }

    public void AddHoovesStamina(int amount)
    {
        // Check to see if player HoovesStamina is low
        if (playerHoovesStamina < maxPlayerStamina)
        {
            // If player Hooves Stamina is down, add stamina
            playerHoovesStamina = playerHoovesStamina + amount;
            if (playerHoovesStamina > maxPlayerStamina)
            {
                playerHoovesStamina = maxPlayerStamina;
               // Debug.Log("Player Hooves Stamina is maxed");
            }

            // Calculate the percentage Hooves Stamina for the bar
            percentHoovesStamina = playerHoovesStamina / maxPlayerStamina;
            //Debug.Log("Percent Hooves Stamina is " + percentHoovesStamina);

            // Update the HUD image
            GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

        }
    }

    public void AddBarbedStamina(int amount)
    {
        // Check to see if player HoovesStamina is low
        if (playerBarbedStamina < maxPlayerStamina)
        {
            // If player Barbed Stamina is down, add stamina
            playerBarbedStamina = playerBarbedStamina + amount;

            if (playerBarbedStamina > maxPlayerStamina)
            {
                playerBarbedStamina = maxPlayerStamina;
                // Debug.Log("Player Hooves Stamina is maxed");
            }

            // Calculate the percentage Barbed Stamina for the bar
            percentBarbedStamina = playerBarbedStamina / maxPlayerStamina;
            //Debug.Log("Percent Hooves Stamina is " + percentHoovesStamina);

            // Update the HUD image
            GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

        }
    }

    public void AddBladesStamina(int amount)
    {
        // Check to see if player HoovesStamina is low
        if (playerBladesStamina < maxPlayerStamina)
        {
            // If player Stamina is down, add regen it
            playerBladesStamina = playerBladesStamina + amount;

            if (playerBladesStamina > maxPlayerStamina)
            {
                playerBladesStamina = maxPlayerStamina;
                // Debug.Log("Player Hooves Stamina is maxed");
            }

            // Calculate the percentage Barbed Stamina for the bar
            percentBladesStamina = playerBladesStamina / maxPlayerStamina;
            //Debug.Log("Percent Hooves Stamina is " + percentHoovesStamina);

            // Update the HUD image
            GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

        }
    }

    public void UpdateBoss()
    {
        // Open access to boss door inside the Hub

        bossWin = true;

        SingletonGameController.Instance.CompleteBoss((levelNumber - 1));

        // Open door back to Locker Room when Boss is dead

        GameObject.Find("DoorToLockerRoom").GetComponent<MeshRenderer>().enabled = true;

        // Stop timer
        CancelInvoke("CountdownTimer");

        // Play the round bell
        SingletonAudioSource.Instance.PlayEffect(7);

        // Play fireworks

        if (levelNumber == 3)
        {
            GameObject[] WinnerFireworks = GameObject.FindGameObjectsWithTag("Fireworks");
            foreach (GameObject GO in WinnerFireworks)
            {
                GO.GetComponent<ParticleSystem>().Play();
            }
        }

        // Update the HUD
        GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

    }

  

    public void Die()               // Player has died...
    {
        // Decrement playerLives by 1
        playerLives--;

        // Check if player is out of lives
        if (playerLives <= 0)
        {
            // If yes, Load last save
            SceneManager.LoadScene("10_Locker_Room_Hub");
        }

        else // player has died but is not out of lives
        {
            // Teleport player to spawn point in level
            player.transform.position = respawnPoint;

            InitializeRound();

            // Update HUD
            GameObject.Find("Canvas HUD").GetComponent<HUDController>().UpdateHUD(playerLives, percentHealth, percentHoovesStamina, percentBarbedStamina, percentBladesStamina, activeAttack);

        }

    }

    // @ CountdownTimer puts a limit on the time allowed in the round
    // Display time in Mins:seconds:tenths

    void CountdownTimer()
    {
        

        timeLeft -= 0.1f;
        int timeTenthsSeconds = (int)((timeLeft * 10.0f) % 60.0f);
        int timeSeconds = (int)((timeLeft * 10.0f) / 10.0f) % 60;
        int timeMinutes = (int)((timeLeft * 10.0f) / 10.0f)/60;
        timerText.text =  timeMinutes + ":" + timeSeconds + ":" + timeTenthsSeconds;

        if (timeLeft <= 0.0f)
        {
            

            CancelInvoke("CountdownTimer");

    

            // Kill the player cause they lost this round but let them hear the bell
            Invoke("Die", 0.1f);


            

        }


    }


}
