﻿using UnityEngine;
using System.Collections;

/**
    @author Darrick Hilburn
    @date 9/9/2016

    This script holds the code for making an enemy move.
    */
public class EnemyMove : MonoBehaviour
{
    // Movement is an enum that holds values for
    //    the different movement options available
    enum Movement
    {
        WAIT,           // Do nothing
        FORWARD,        // Move forward
        FLANK_LEFT,     // Move left, face forward
        LEFT,           // Turn and move left
        FLANK_RIGHT,    // Move right, face forward
        RIGHT,          // Turn and move right
        BACKSTEP,       // Move back, face forward
        RETREAT         // Turn and move back
    }

    [Tooltip("How far the enemy will move.")]
    [Range(0.1f, 3f)]
    public float moveDistance;
    [Tooltip("How long it takes the enemy to move. A higher speed means less time, and vice versa!")]
    [Range(0.5f, 2.5f)]
    public float moveSpeed;
    [Tooltip("How long it takes the enemy to turn. A higher speed means less time, and vice versa!")]
    [Range(0.5f, 2.5f)]
    public float turnSpeed;
    [Tooltip("How long the enemy will wait when its AI tells it to wait.")]
    [Range(0, 5)]
    public float waitTime;
    [Tooltip("How long the enemy will be stunned for when it's stunned.")]
    [Range(0, 10)]
    public float stunTime;
    [Tooltip("How many horizontal positions the enemy can go out to from their starting point.")]
    [Range(0, 5)]
    public int maxHorizontalMoves;
    [Tooltip("How many vertical positions the enemy can go out to from their starting point.")]
    [Range(0, 5)]
    public int maxVerticalMoves;
    
    public float stun;

    // Current horizontal displacement from start
    int horizontalPos;
    // Current vertical displacement from start
    int verticalPos;
    // Flag for if the enemy is moving
    bool moving;

    // What move the enemy is making
    Movement move;
    // Reference to the previous position, used for stun checking
    Vector3 initialPos;
    // where the enemy will move to
    Vector3 newPos;
    // Where the enemy will face if they'll be turning
    Quaternion newRotation;

    // Have the enemy start by detemining whether they move or wait.
    void Start()
    {
        horizontalPos = 0;
        verticalPos = 0;
        initialPos = transform.position;
        ConfirmConstraints();
        DetermineMove();
    }

    // When the enemy is not moving, determine if they move or wait.
    void Update()
    {
        if (!moving)
        {
            initialPos = transform.position;
            DetermineMove();
        }
        if (moving && stun >= 100)
        {
            StartCoroutine(Stun());
            StartCoroutine(StunWearoff());
        }
    }

    /**
        DetermineMove randomly determines a boolean to make an enemy either move or wait.
        The option chosen is reflected in the Unity Debug log.
        */
    void DetermineMove()
    {

        if (maxHorizontalMoves.Equals(0) && maxVerticalMoves.Equals(0))
        {
            StartCoroutine(Wait());
        }
        else
        {
            bool moveCheck;
            // 20% chance to not move
            int moveDetermine = Random.Range(0, 5);
            //Debug.Log("Move determine value is " + moveDetermine.ToString());
            if (moveDetermine.Equals(0))
                moveCheck = false;
            else
                moveCheck = true;

            //if (moveCheck)
            //    Debug.Log("Enemy is moving!");
            //else
            //    Debug.Log("Enemy is waiting!");

            if (moveCheck) StartCoroutine(MoveCoroutine());
            else StartCoroutine(Wait());
        }
    }

    IEnumerator MoveCoroutine()
    {
        moving = true;
        //Debug.Log("Current position: " + transform.position.ToString());
        newPos = GetNewPosition();
        //Debug.Log("New position: " + newPos.ToString());

        //Debug.Log("Starting move!");
        // Turn in the appropriate direction if necessary
        if (move == Movement.LEFT)
        {
            newRotation = Quaternion.LookRotation(Vector3.left);
        }
        else if (move == Movement.RIGHT)
        {
            newRotation = Quaternion.LookRotation(Vector3.right);
        }
        else if (move == Movement.RETREAT)
        {
            newRotation = Quaternion.LookRotation(Vector3.back);
        }
        else
        {
            newRotation = Quaternion.LookRotation(Vector3.zero);
        }
        if (newRotation != Quaternion.LookRotation(Vector3.zero))
        {
            while (Quaternion.Angle(transform.rotation, newRotation) > 0.2f)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, turnSpeed * Time.deltaTime);
                yield return null;
            }
        }
        // Move!
        while (Vector3.Distance(transform.position, newPos) > 0.1f)
        {
            transform.position = Vector3.Lerp(transform.position, newPos, moveSpeed * Time.deltaTime);
            yield return null;
        }
        //Debug.Log("Move complete!");
        // Check where the enemy is at for debug reasons
        CheckConstraintsToPos();
        if (move == Movement.LEFT || move == Movement.RIGHT || move == Movement.RETREAT)
        {
            newRotation = Quaternion.LookRotation(Vector3.forward);
            while (Quaternion.Angle(transform.rotation, newRotation) > 0.2f)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, turnSpeed * Time.deltaTime);
                yield return null;
            }
        }
        yield return new WaitForSeconds(1f);
        //Debug.Log("Coroutine complete!");
        moving = false;
    }

    Vector3 GetNewPosition()
    {
        move = GetMoveTypeSmart();

        switch (move)
        {
            case (Movement.FLANK_LEFT):
            case (Movement.LEFT):
                return new Vector3(transform.position.x - moveDistance, transform.position.y, transform.position.z);
            case (Movement.FLANK_RIGHT):
            case (Movement.RIGHT):
                return new Vector3(transform.position.x + moveDistance, transform.position.y, transform.position.z);
            case (Movement.BACKSTEP):
            case (Movement.RETREAT):
                return new Vector3(transform.position.x, transform.position.y, transform.position.z - moveDistance);
            default:
                return new Vector3(transform.position.x, transform.position.y, transform.position.z + moveDistance);
        }
    }

    Movement GetMoveTypeSimple()
    {
        int movement = Random.Range(0, 7);
        switch (movement)
        {
            case (1):
            case (2):
                return Movement.FLANK_RIGHT;
            case (3):
            case (4):
                return Movement.FLANK_LEFT;
            case (5):
            case (6):
                return Movement.BACKSTEP;
            default:
                return Movement.FORWARD;
        }
    }

    Movement GetMoveTypeSmart()
    {
        int movement;

        #region Column Movement
        // Check if the enemy only moves in a column
        if (maxHorizontalMoves.Equals(0))
        {
            // First check if the enemy has moved out as far as they can
            // >= used as a catch for if the enemy somehow moves past the constraint
            if (Mathf.Abs(verticalPos) >= maxVerticalMoves)
            {
                // Check where the enemy is
                // positive = forward
                // negative = backwards

                // Positive: Enemy is as far forward as possible
                // They can only move backwards
                if (verticalPos > 0)
                {
                    movement = Random.Range(0, 2);
                    if (movement.Equals(0))
                    {
                        verticalPos--;
                        return Movement.BACKSTEP;
                    }
                    else
                    {
                        verticalPos--;
                        return Movement.RETREAT;
                    }
                }
                // Negative: Enemy is as far backwards as possible
                // They can only move forwards
                else
                {
                    verticalPos++;
                    return Movement.FORWARD;
                }
            }
            // If the enemy is not at an end, they can freely move up or down
            else
            {
                movement = Random.Range(0, 3);
                switch (movement)
                {
                    case (1):
                        verticalPos--;
                        return Movement.BACKSTEP;
                    case (2):
                        verticalPos--;
                        return Movement.RETREAT;
                    default:
                        verticalPos++;
                        return Movement.FORWARD;
                }
            }
        }
        #endregion

        #region Row Movement
        // Check if the enemy moves in a row
        else if (maxVerticalMoves.Equals(0))
        {
            // First check if the enemy has moved out as far as they can
            if (Mathf.Abs(horizontalPos) >= maxHorizontalMoves)
            {
                // Check where the enemy is
                // positive = right
                // negative = left

                // Positive: Enemy is as far right as possible
                // They can only move left
                if (horizontalPos > 0)
                {
                    movement = Random.Range(0, 2);
                    if (movement.Equals(0))
                    {
                        horizontalPos--;
                        return Movement.FLANK_LEFT;
                    }
                    else
                    {
                        horizontalPos--;
                        return Movement.LEFT;
                    }
                }
                // Negative: Enemy is as far left as possible
                // They can only move right
                else
                {
                    movement = Random.Range(0, 2);
                    if (movement.Equals(0))
                    {
                        horizontalPos++;
                        return Movement.FLANK_RIGHT;
                    }
                    else
                    {
                        horizontalPos++;
                        return Movement.RIGHT;
                    }
                }
            }
            // If the enemy is not at an end, they may move left or right
            else
            {
                movement = Random.Range(0, 4);
                switch (movement)
                {
                    case (0):
                        horizontalPos++;
                        return Movement.FLANK_RIGHT;
                    case (1):
                        horizontalPos++;
                        return Movement.RIGHT;
                    case (2):
                        horizontalPos--;
                        return Movement.FLANK_LEFT;
                    case (3):
                        horizontalPos--;
                        return Movement.LEFT;
                }
            }
        }
        #endregion

        #region Grid Movement
        // If enemy moves in neither a row or column, it has a grid to move in.
        else
        {
            #region Horizontal Edge
            // Enemy is on a horizontal edge
            if (Mathf.Abs(horizontalPos) >= maxHorizontalMoves)
            {
                #region Corner Movement
                // Enemy is in a corner
                if (Mathf.Abs(verticalPos) >= maxVerticalMoves)
                {
                    // Forward-Right corner
                    if (horizontalPos > 0 && verticalPos > 0)
                    {
                        movement = Random.Range(0, 4);
                        switch (movement)
                        {
                            case (0):
                                horizontalPos--;
                                return Movement.FLANK_LEFT;
                            case (1):
                                horizontalPos--;
                                return Movement.LEFT;
                            case (2):
                                verticalPos--;
                                return Movement.BACKSTEP;
                            case (3):
                                verticalPos--;
                                return Movement.RETREAT;
                        }
                    }
                    // Back-right corner
                    else if (horizontalPos > 0 && verticalPos < 0)
                    {
                        movement = Random.Range(0, 3);
                        switch (movement)
                        {
                            case (1):
                                horizontalPos--;
                                return Movement.FLANK_LEFT;
                            case (2):
                                horizontalPos--;
                                return Movement.LEFT;
                            default:
                                verticalPos++;
                                return Movement.FORWARD;
                        }
                    }
                    // Back-left corner
                    else if (horizontalPos < 0 && verticalPos < 0)
                    {
                        movement = Random.Range(0, 3);
                        switch (movement)
                        {
                            case (1):
                                horizontalPos++;
                                return Movement.FLANK_RIGHT;
                            case (2):
                                horizontalPos++;
                                return Movement.RIGHT;
                            default:
                                verticalPos++;
                                return Movement.FORWARD;
                        }
                    }
                    // Forward-Left corner
                    else
                    {
                        movement = Random.Range(0, 4);
                        switch (movement)
                        {
                            case (0):
                                horizontalPos++;
                                return Movement.FLANK_RIGHT;
                            case (1):
                                horizontalPos++;
                                return Movement.RIGHT;
                            case (2):
                                verticalPos--;
                                return Movement.BACKSTEP;
                            case (3):
                                verticalPos--;
                                return Movement.RETREAT;
                        }
                    }
                }
                #endregion
                #region Right Edge
                // Enemy is on right edge
                if (horizontalPos > 0)
                {
                    movement = Random.Range(0, 5);
                    switch (movement)
                    {
                        case (1):
                            horizontalPos--;
                            return Movement.FLANK_LEFT;
                        case (2):
                            horizontalPos--;
                            return Movement.LEFT;
                        case (3):
                            verticalPos--;
                            return Movement.BACKSTEP;
                        case (4):
                            verticalPos--;
                            return Movement.RETREAT;
                        default:
                            verticalPos++;
                            return Movement.FORWARD;
                    }
                }
                #endregion
                #region Left Edge
                // Enemy is on left edge
                else
                {
                    movement = Random.Range(0, 5);
                    switch (movement)
                    {
                        case (1):
                            horizontalPos++;
                            return Movement.FLANK_RIGHT;
                        case (2):
                            horizontalPos++;
                            return Movement.RIGHT;
                        case (3):
                            verticalPos--;
                            return Movement.BACKSTEP;
                        case (4):
                            verticalPos--;
                            return Movement.RETREAT;
                        default:
                            verticalPos++;
                            return Movement.FORWARD;
                    }
                }
                #endregion
            }
            #endregion
            #region Vertical Edge
            // Enemy is on a vertical edge
            else if (Mathf.Abs(verticalPos) >= maxVerticalMoves)
            {
                #region Forward Edge
                // Enemy is on forward edge
                if (verticalPos > 0)
                {
                    movement = Random.Range(0, 6);
                    switch (movement)
                    {
                        case (0):
                            horizontalPos++;
                            return Movement.FLANK_RIGHT;
                        case (1):
                            horizontalPos++;
                            return Movement.RIGHT;
                        case (2):
                            horizontalPos--;
                            return Movement.FLANK_LEFT;
                        case (3):
                            horizontalPos--;
                            return Movement.LEFT;
                        case (4):
                            verticalPos--;
                            return Movement.BACKSTEP;
                        case (5):
                            verticalPos--;
                            return Movement.RETREAT;
                    }
                }
                #endregion
                #region Back Edge
                // Enemy is on back edge
                else
                {
                    movement = Random.Range(0, 5);
                    switch (movement)
                    {
                        case (1):
                            horizontalPos++;
                            return Movement.FLANK_RIGHT;
                        case (2):
                            horizontalPos++;
                            return Movement.RIGHT;
                        case (3):
                            horizontalPos--;
                            return Movement.FLANK_LEFT;
                        case (4):
                            horizontalPos--;
                            return Movement.LEFT;
                        default:
                            verticalPos++;
                            return Movement.FORWARD;
                    }
                }
                #endregion
            }
            #endregion
            #region Free Move
            // Enemy is in the middle of the grid
            else
            {
                movement = Random.Range(0, 7);
                switch (movement)
                {
                    case (1):
                        horizontalPos++;
                        return Movement.FLANK_RIGHT;
                    case (2):
                        horizontalPos++;
                        return Movement.RIGHT;
                    case (3):
                        horizontalPos--;
                        return Movement.FLANK_LEFT;
                    case (4):
                        horizontalPos--;
                        return Movement.LEFT;
                    case (5):
                        verticalPos--;
                        return Movement.BACKSTEP;
                    case (6):
                        verticalPos--;
                        return Movement.RETREAT;
                    default:
                        verticalPos++;
                        return Movement.FORWARD;
                }
            }
            #endregion
        }
        #endregion

        // This return only reached if somehow no other returns reached.
        Debug.Log("AN ERROR HAS OCCURRED");
        return 0;
    }

#if UNITY_EDITOR
    void OnGUI()
    {
        GUIStyle blackFont = new GUIStyle();
        blackFont.normal.textColor = Color.black;
        GUILayout.BeginVertical();
        GUILayout.Label("Horizontal position: " + Mathf.Abs(horizontalPos) + " / " + maxHorizontalMoves, blackFont);
        GUILayout.Label("Vertical position: " + Mathf.Abs(verticalPos) + " / " + maxVerticalMoves, blackFont);
        GUILayout.EndVertical();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Move type: ", blackFont);
        switch(move)
        {
            case (Movement.WAIT):
                GUILayout.Label("Wait");
                break;
            case (Movement.FORWARD):
                GUILayout.Label("Forward", blackFont);
                break;
            case (Movement.FLANK_LEFT):
                GUILayout.Label("Sidestep Left", blackFont);
                break;
            case (Movement.LEFT):
                GUILayout.Label("Turn Left", blackFont);
                break;
            case (Movement.FLANK_RIGHT):
                GUILayout.Label("Sidestep right", blackFont);
                break;
            case (Movement.RIGHT):
                GUILayout.Label("Turn right", blackFont);
                break;
            case (Movement.BACKSTEP):
                GUILayout.Label("Step back", blackFont);
                break;
            case (Movement.RETREAT):
                GUILayout.Label("RUN TO THE HILLS!!!", blackFont);
                break;
        }
        GUILayout.EndHorizontal();
    }
#endif

    IEnumerator Wait()
    {
        moving = true;
        //Debug.Log("Waiting . . . . . . ");
        yield return new WaitForSeconds(waitTime);
        //Debug.Log("Wait complete!");
        yield return new WaitForSeconds(1f);
        moving = false;
    }

    IEnumerator Stun()
    {
        transform.Rotate(Vector3.up);
        newRotation = Quaternion.LookRotation(Vector3.forward);
        while (Vector3.Distance(transform.position, initialPos) > 0.1f)
        {
            Vector3.Lerp(transform.position, initialPos, Time.deltaTime);
            yield return null;
        }
        yield return new WaitForSeconds(stunTime);
    }

    IEnumerator StunWearoff()
    {
        stun = 0;
        while (Quaternion.Angle(transform.rotation, newRotation) > 0.2f)
        {
            Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime);
            yield return null;
        }
    }

    void ConfirmConstraints()
    {
        if (maxHorizontalMoves == 0 && maxVerticalMoves == 0)
            Debug.Log("Enemy can't move!");
        else if (maxHorizontalMoves == 0)
            Debug.Log("Enemy movement constrained to a column");
        else if (maxVerticalMoves == 0)
            Debug.Log("Enemy movement constrained to a row");
        else
            Debug.Log("Enemy can move in a grid");
    }

    void CheckConstraintsToPos()
    {

        if (Mathf.Abs(verticalPos) >= maxVerticalMoves && maxVerticalMoves != 0)
        {
            if (verticalPos > 0)
            {
                Debug.Log("Enemy is as far forward as possible");
            }
            else
            {
                Debug.Log("Enemy is as far back as possible");
            }
        }
        else if (Mathf.Abs(horizontalPos) >= maxHorizontalMoves && maxHorizontalMoves != 0)
        {
            if (horizontalPos > 0)
            {
                Debug.Log("Enemy is as far right as possible");
            }
            else
            {
                Debug.Log("Enemy is as far left as possible");
            }
        }
        else if (Mathf.Abs(horizontalPos) >= maxHorizontalMoves && Mathf.Abs(verticalPos) >= maxVerticalMoves && maxHorizontalMoves != 0 && maxVerticalMoves != 0)
        {
            if (horizontalPos > 0 && verticalPos > 0)
                Debug.Log("Enemy is in forward-right corner");
            else if (horizontalPos > 0 && verticalPos < 0)
                Debug.Log("Enemy is in back-right corner");
            else if (horizontalPos < 0 && verticalPos > 0)
                Debug.Log("Enemy is in forward-left corner");
            else
                Debug.Log("Enemy is in back-left corner");
        }
    }
}
