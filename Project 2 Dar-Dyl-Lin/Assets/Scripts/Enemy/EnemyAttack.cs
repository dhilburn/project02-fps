﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    enum AttackType
    {
        NO_ATTACK,
        MELEE,
        PROJECTILE
    }

    [SerializeField]
    GameObject projectile;
    [SerializeField]
    float cooldownTime;
    [SerializeField]
    float waitTime;
    [SerializeField]
    [Range(0, 9)]
    int meleeChance;

    public float projectileForce;
    public float meleeDamage;
    public float projectileDamage;

    [HideInInspector]
    public bool isAttacking;
    AttackType attack;

    // Use this for initialization
    void Start()
    {
        isAttacking = false;
        DetermineAttack();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAttacking)
            DetermineAttack();
    }

    void DetermineAttack()
    {
        int attackType = Random.Range(0, 5);
        if (attackType.Equals(0))
        {
            StartCoroutine(NoAttack());
        }
        else
        {
            attackType = Random.Range(0, 2);
            if (attackType.Equals(0))
                attack = AttackType.MELEE;
            else
                attack = AttackType.PROJECTILE;
            StartCoroutine(PerformAttack());
        }
    }

    IEnumerator PerformAttack()
    {

        switch (attack)
        {
            case (AttackType.MELEE):
            case (AttackType.PROJECTILE):
                StartCoroutine(ProjectileAttack());
                break;
            default:
                StartCoroutine(NoAttack());
                break;
        }
        yield return null;
    }

    IEnumerator MeleeAttack()
    {
        // Melee attack removed - animation not functioning properly
        yield return null;
    }

    IEnumerator ProjectileAttack()
    {
        isAttacking = true;
        Debug.Log("pop");
        GetComponentInChildren<SpawnProjectiles>().Spawn(projectile);
        yield return new WaitForSeconds(cooldownTime);
        isAttacking = false;
    }

    IEnumerator NoAttack()
    {
        isAttacking = true;
        Debug.Log("ATTACKING IS FOR SCRUBS");
        yield return new WaitForSeconds(waitTime);
        isAttacking = false;
    }
}
