﻿using UnityEngine;
using System.Collections;

public class EnemyProjectileController : MonoBehaviour 
{
    [Tooltip("Force acting on the projectile to make it move.")]
    [Range(10, 500)]
    public float force;

    void Start()
    {
        GetComponent<Rigidbody>().AddForce(Vector3.forward * force);
    }
    void Update()
    {
        if (transform.position.y < 0)
            Destroy(gameObject);
    }

    void OnCollisionEnter(Collision col)
    {
        Destroy(gameObject);
    }
}
