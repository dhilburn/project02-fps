﻿using UnityEngine;
using System.Collections;

public class SpawnProjectiles : MonoBehaviour 
{
    public void Spawn(GameObject projectile)
    {
        Instantiate(projectile);
    }
}
